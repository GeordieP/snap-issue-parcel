- `yarn start` or `npm start` to run in parcel dev mode

- `yarn clean` or `npm run clean` to remove the parcel dist/ (dev mode), build/, and .cache/ directories

- `yarn build` or `npm run build` to run `clean` and build the app with parcel and run react-snap

This repo works; react-snap does its thing and we get a rendered site out, along with JS running on the page when enabled.

Contrast with [gitlab.com/GeordieP/snap-issue-cra](https://gitlab.com/GeordieP/snap-issue-cra), the create-react-app version, in which the JS doesn't behave as it should (and as it does here).
