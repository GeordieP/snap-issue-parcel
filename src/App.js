import React from 'react';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { counter: 0 };
  }

  componentDidMount() {
    // counter+1 every second
    setInterval(
      () => this.setState({ counter: this.state.counter + 1}),
      1000
    );
  }

  render() {
    return (
      <div className="App">
        <button onClick={ () => alert('Hello!') }>Click me</button>
        <p>{this.state.counter}</p>
      </div>
    );
  }
}
